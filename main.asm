%include "words.inc"

%define buffer_size 256

section .data
start_msg: 
	db "Write key word", 10, 0
found_msg: 
	db "Value found: ", 0
err_msg_not_found:
	db "Error: word not found", 10, 0
err_msg_giant_word:
	db "Error: word is bigger than buffer", 10, 0

section .text

global _start
extern read_word, find_word, print_string, print_newline, exit, print_err, string_length

_start:
	mov rdi, start_msg			; указатель на сообщение в rdi
	call print_string 			; вывод сообщения
	push rbp					; rbp is calee-saved
	sub rsp, 256				; освобождаем место в стеке для буфера (+1 для 0-терм)
	mov rbp, rsp				; сохраняем позицию стека в rbp (rsp - calee-saved)
	mov rsi, 255				; размер буфера 
	mov rdi, rsp				; адрес начала буфера
	call read_word				;
	cmp rax, 0					; если вернулся ноль, то
	je .word_too_long			; буфера не хватила для слова
	mov rdi, rax				; адрес буфера возвращён в rax, помещаем в rdi для поиска слова
	mov rsi, link_next			; начало словаря в rsi
	call find_word
	test rax, rax				; если не нашлось, будет возвращён ноль
	jz .notFound 				; выводим сообшение
	mov rsp, rbp				; восстанавливаем указатель стека
	pop rbp						; восстанавливаем rbp
	add rax, 8					; переходим к началу ключа
	mov r10, rax				; адрес начала ключа в rax, сохраним его в r10
	mov rdi, rax				; указатель на строку(адрес начала вхождения ключа в словарь)
	call string_length			; и вычислим длину ключа
	mov r9, rax
	inc r9						; сохраним длину(+ 0term) ключа в r9
	mov rdi, found_msg			; указатель на сообщение что слово найдено
	call print_string 			; выводим
	add r10, r9					; переходим к первому байту значения
	mov rdi, r10				; помещаем адрес значения по ключу в аргумент
	call print_string 			; выводим значение по ключу
	call print_newline			; переносим строку
	mov rdi, 0					; код возврата
	call exit					; 
.word_too_long:
	mov rsp, rbp				; восстанавливаем указатель стека
	pop rbp						; восстанавливаем rbp
	mov rdi, err_msg_giant_word	; указатель на сообщение
	call print_err_string		; 
	mov rdi, 1					; код возврата
	call exit					; 
.notFound:
	mov rsp, rbp				; восстанавливаем указатель стека
	pop rbp						; восстанавливаем rbp
	mov rdi, err_msg_not_found	; указатель на сообщение
	call print_err_string		; 
	mov rdi, 1					; код возврата
	call exit 

print_err_string:
	call string_length 		; возвращает длину в rax строки, указатель на которую находится в rdi 
	mov rdx, rax			; size_t count
	mov rax, 1				; sys_write code
	mov rsi, rdi			; The address of the first byte in a sequence of bytes to be written
	mov rdi, 2				; stderr 
	syscall					;
    	ret
